<?php
namespace Vues;

class indexView {

    protected $actionToShow;

    public function __construct($actionToShow){
       $this->actionToShow = $actionToShow;
    }

    public function render(){

        $elementToRender = "UnAuthorized";

        switch ($this->actionToShow) {

            
            case 'stop':
                $elementToRender = $this->stop404();

        }

        echo <<<EOF
      <!DOCTYPE html>
<html lang="fr">
<!-- HEAD -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <link href="./src/web/style.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <title>WishList</title>
</head>
<body>
  <a href="https://icons8.com/icon/53787/wedding-gift"></a> <!-- FIN HEAD -->
   <!-- BODY-->
   <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
    </li>
   $elementToRender <!-- element à renvoyer -->
  <footer class="pagefooter">
    <!-- FOOTER -->
    <div class="myquote">
      <blockquote>
        <!-- avis  -->
        <span class="quote">“</span> My Wishlist est très simple d'utilisation et vous permet de créer et gérer vos listes en un clin d'oeil, un incontournable pour les anniversaires, mariages et naissances.<br>
        <em>Elvina, utilisatrice depuis 3 ans</em>
      </blockquote>
    </div><!-- fin  -->
    <div class="menu">
     <div class="row">
        <div class="col-md-12">
          <h3 class="text-center">MyWishlist</h3>
          <img src="https://img.icons8.com/cotton/64/000000/wedding-gift.png" class="gitfbox" alt='wishlist'>
          </div>
      </div><!-- FIN DIV row-->
    </div><!-- FIN DIV Menu-->
    <div class="footer-bottom text-center">
      <div class = "Mywish">
        <p><span>© 2019,MyWishlist, Privacy Policy,Terms & Conditions        
          </span>
          </p>
      </div><!-- FIN DIV Mywish-->
    </div> <!-- FIN DIV FOOTER BOTTOM-->
  </footer>
  <script>
window.addEventListener('scroll', function (e) {
        var mynav = document.getElementById('mynav');
        if (document.documentElement.scrollTop || document.body.scrollTop > window.innerHeight) {
                mynav.classList.add('nav-colored');
                mynav.classList.remove('nav-transparent');
            } else {
                mynav.classList.add('nav-transparent');
                mynav.classList.remove('nav-colored');
            }
    });
    </script>
</body>
<!-- FIN BODY-->
</html><!-- FIN HTML-->         
EOF;
        
    }

    public function stop404(){
      return "<div class = 'text-center'>
              Vous n'avez pas la permission
              </div>";
    }
}

?>