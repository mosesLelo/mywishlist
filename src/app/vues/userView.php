<?php
namespace Vues;

class userView
{

  protected $actionToShow;

  public function __construct($actionToShow)
  {
    $this->actionToShow = $actionToShow;
  }


  public function render()
  {

    $elementToRender = "";

    switch ($this->actionToShow) {
      case 'inscription':
        $elementToRender = $this->registration();
        break;
      case 'connexion':
        $elementToRender = $this->login();
        break;
      case 'moncompte':
        $elementToRender = $this->homeConnected();
        break;
      case 'editUser':
        $elementToRender = $this->editUser();
        break;
      case 'linkliste':
        $elementToRender = $this->linkListe();
        break;
      case 'getParticipation':
        $this->getParticipation();
        break;
    }

    echo <<<EOF
      <!DOCTYPE html>
<html lang="fr">
<!-- HEAD -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <link href="./src/web/style.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <a href="https://icons8.com/icon/53787/wedding-gift"></a>
  <title>WishList</title>
</head>
<!-- FIN HEAD -->
<body>
  <!-- BODY-->
  $elementToRender
  <!-- element à renvoyer -->
  <footer class="pagefooter">
    <!-- FOOTER -->
    <div class="myquote">
      <blockquote>
        <!-- avis  -->
        <span class="quote">“</span> My Wishlist est très simple d'utilisation et vous permet de créer et gérer vos listes en un clin d'oeil, un incontournable pour les anniversaires, mariages et naissances.
        <br>
        <em>Elvina, utilisatrice depuis 3 ans</em>
      </blockquote>
    </div> <!-- FIN DIV myquote-->
    <!-- fin  -->
    <div class="menu">
      <div class="row">
        <div class="col-md-12">
          <h3 class="text-center">MyWishlist</h3>
          <img src="https://img.icons8.com/cotton/64/000000/wedding-gift.png" class="gitfbox" alt='wishlist'>
          </div>
      </div><!-- FIN DIV row-->
    </div><!-- FIN DIV Menu-->
    <div class="footer-bottom text-center">
      <div class = "Mywish">
        <p><span>© 2019,MyWishlist, Privacy Policy,Terms & Conditions        
          </span>
          </p>
      </div><!-- FIN DIV Mywish-->
    </div> <!-- FIN DIV FOOTER BOTTOM-->
  </footer>
   <script>
window.addEventListener('scroll', function (e) {
        var mynav = document.getElementById('mynav');
        if (document.documentElement.scrollTop || document.body.scrollTop > window.innerHeight) {
                mynav.classList.add('nav-colored');
                mynav.classList.remove('nav-transparent');
            } else {
                mynav.classList.add('nav-transparent');
                mynav.classList.remove('nav-colored');
            }
    });
    </script>
</body>
<!-- FIN BODY-->
</html><!-- FIN HTML-->

EOF;
  }

  public function registration()
  {

    return "
        <header>
  <!-- NAVBAR -->
  <ul class='nav nav-pills fixed-top' id ='mynav'>
    <li class='nav-item'>
      <a class='navbar-brand' href='#'> <img src='https://img.icons8.com/cotton/64/000000/wedding-gift.png' alt= 'wishlist'  height = '30' class ='giftbox'> MyWishList</a>
    </li>
    <li class='nav-item'><a class='nav-link' href='/mywishlist'>Home</a></li>
    <li class='nav-item'><a class='nav-link' href='./createListe'>Créer une liste</a></li>
  </ul>
</header>
<!-- jumbotron -->
<div class='jumbotron jumbotron-fluid' id= 'jumbotronConnex'>
  <h1 class='text-center'>Créer un compte</h1>
</div>
<!--FIN JUMBOTRON-->
</div>
<div class='wrapper'>
  <div class='row'>
    <div class='col-sm-3 ' id='leftside'>
      <h3 class='text-center'>Comment faire ?</h3>
      <p>
        Pour créer votre compte, rien de plus simple, choississez un pseudo et un mot de passe sûr et en quelques clics votre compte sera créé. Pas encore décidé ? Rendez-vous sur la
        <a href='/mywishlist/' class='lien'>page d'accueil</a> pour retrouver nos avantages.
      </p>
    </div>
    <div class='col-sm-7'>
      <!-- zone de connexion -->
      <form action= './register' method='POST'>
        <div class='rowTab'>
          <div class='labels'>
            <label for='login' class='label'>Pseudo*</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='login' id='r_lgn' class='input-field' placeholder='Entrez votre pseudo' maxlength='20' required>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='lastname' class='label'>Nom*</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='lastname' placeholder='Entrez votre nom' class='input-field' required>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='firstname' class='label'>Prénom*</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='firstname' placeholder='Entrez votre prénom' class='input-field' required>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='password' class='label'>Mot de passe* (6 caractères minimum)</label>
          </div>
          <div class='rightTab'>
            <input type='password' pattern='.{6,}' required title='6 caractères minimum' name='pass1' placeholder='Entrez un mot de passe' class='input-field' required>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='pass2' class='label'>Confirmation du mot de passe</label>
          </div>
          <div class='rightTab'>
            <input type='password' name='pass2' placeholder='Confirmez mot de passe' class='input-field' required>
          </div>
        </div>
        <div class='bton'>
          <input class='submit' type='submit' value='Inscription' id= 'submit'>
        </div>
      </form>
    </div>
    </div>
</div>";
  }
  // Méthode pour page mon compte pour connectés
  public function homeConnected()
  {

    if (isset($_SESSION['user'])) {
      $firstname = $_SESSION['user']['firstname'];
      $lastname  = $_SESSION['user']['lastname'];
      $nbList = 0;
    } else {
      $firstname = "";
      $lastname = "";
    }


    return "
        
         <header>
    <!-- NAVBAR -->
    <ul class=' nav nav-pills fixed-top' id ='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png' alt = 'wishlist'> MyWishList</a>
      </li>
      <li class='nav-item'>
      <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/userListe'>Mes listes</a>
      </li>
       <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/logout'>Deconnexion</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
 </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronConnex'>
    <h1 class='text-center'>Mon Compte</h1>
  </div><!--FIN JUMBOTRON-->
  <!-- PROFIL -->
  <div class='profil'>
        <div class = 'titleprof'>
    <h2 class='heading text-center'>Bienvenue $firstname $lastname !</h2>
  </div><!-- FIN PROFIL -->
  <!--ElEMENT ABSTRAIT  -->
  <div class='text-center'>
    <a href='./userLinkListe' >lier une liste</a> | 
    <a href='./userParticipation'> voir participation</a>
  </div><!-- FIN ELEMENT ABSTRAIT -->
  <!-- wrapper -->
  <div class='mychoices'>
  <div class = titre>
  <h2 class='text-center'>Que voulez-vous faire ?</h2>
   </div>
    <div class = 'row'>    
      <div class='col-md-3'>
        <img src='https://img.icons8.com/plasticine/100/000000/wish-list.png' alt = 'meslistes'>
        <p><a class='btn btn-info' href='./userListe' role='button'>Mes listes</a></p>
        </div>
      <div class='col-md-3 ' ><!--  col -->       
           <img src='https://img.icons8.com/clouds/100/000000/todo-list.png' alt='creerliste'>
        <p><a class='btn btn-info' href='./createListe' role='button'>Créer une Liste</a></p>    
      </div><!-- fin col -->
      <div class='col-md-3' >  
      <img src='https://img.icons8.com/clouds/100/000000/edit-property.png' alt = 'modify'>
        <p><a class='btn btn-info' href='./editUser' role='button'>Modifier Compte</a></p>
    </div>
    <div class='col-md-3' >
      <img src='https://img.icons8.com/clouds/100/000000/trash.png'alt = 'delete'>
        <p><a class='btn btn-info' href='./deleteAccount' role='button'>Supprimer Compte</a></p>
    </div>
    </div>
  </div>";
  }



  public function login()
  {

    if (isset($_COOKIE['login']) && isset($_COOKIE['pass'])) {

      $login = $_COOKIE['login'];
      $passwd = $_COOKIE['pass'];
    } else {
      $login = "";
      $passwd = "";
    }

    return "
       <header>
  <!-- NAVBAR -->
  <ul class='nav nav-pills fixed-top' id ='mynav'>
    <li class='nav-item'>
      <a class='navbar-brand' href='#'> <img src='https://img.icons8.com/cotton/64/000000/wedding-gift.png' alt= 'wishlist'  height = '30' class ='giftbox'> MyWishList</a>
    </li>
    <li class='nav-item'><a class='nav-link' href='/mywishlist'>Home</a></li>
    <li class='nav-item'><a class='nav-link' href='createListe'>Créer une liste</a></li>
  </ul></header>
<!-- jumbotron -->
<div class='jumbotron jumbotron-fluid' id= 'jumbotronConnex'>
  <h1 class='text-center'>Connexion</h1>
</div>
<!--FIN JUMBOTRON-->
</div>
<div class='wrapper'>
  <div class='row'>
    <div class='col-sm-3' id='leftside'>
      <h3 class='text-center'>Comment faire ?</h3>
      <p>
        Connectez-vous pour bénéficier de tous les avantages de myWishlist. Si vous ne possédez pas de compte, vous pouvez vous inscrire par  <a href='register' class ='lien2'>ici</a>. Vous pouvez également visiter <a href='/mywishlist' class='lien'> la page d'accueil</a> pour retrouver la liste de nos avantages.
      </p>
    </div>
    <div class='col-sm-7'>
      <form action='./login' method='post'>
        <div class='rowTab'>
          <div class='labels'>
            <label for='login'>Pseudo</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='login' value='$login' class='input-field'  placeholder='Entrez votre pseudo'required>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='password '>Mot de passe</label>
          </div>
          <div class='rightTab'>
          <input type='password' name='pass' value='$passwd' placeholder='Entrez votre mot de passe'  class='input-field' required>
        </div>
    </div>
    <div class='rowTab'>
      <div class='labels'>
        <label for='password'>Se souvenir de moi</label>
      </div>
      <div class='rightTab'>
        <input type='checkbox' name='takeCookie' value='saveUser check' class='check'>
      </div>
    </div>
    <div class='bton'>
      <button id='submit' type='submit'>Connexion</button>
      </form>
    </div>
  </div>
</div>";
  }


  public function editUser()
  {

    $user = $_SESSION['currentUser'];

    return "
    <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id ='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'> <img src='https://img.icons8.com/cotton/64/000000/wedding-gift.png' alt= 'wishlist'  height = '30' class ='giftbox'> MyWishList</a>
      </li>
      <li class='nav-item'><a class='nav-link' href='/mywishlist'>Home</a></li>
      <li class='nav-item'><a class='nav-link' href='/mywishlist/logout'>Deconnexion</a></li>
    </ul>
  </header>
  <!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id= 'jumbotronConnex'>
    <h1 class='text-center'>Modifier un compte</h1>
  </div>
  <!--FIN JUMBOTRON-->
  </div>
  <div class='wrapper'>
    <div class='row'>
      <div class='col-sm-3 ' id='leftside'>
        <h3 class='text-center'>Comment faire ?</h3>
        <p>
          Pour modifier votre compte, rien de plus simple, saissisez vos nouvelles informations, modifiez votre mot de passe et validez votre modification.
        </p>
      </div>
      <div class='col-sm-7'>
        <!-- zone de connexion -->
        <form action= '../editUser' method='POST'>
          <div class='rowTab'>
            <div class='labels'>
              <label for='lastname' class='label'>Nom*</label>
            </div>
            <div class='rightTab'>
              <input type='text' name='lastname' placeholder='Entrez votre nom' class='input-field' value='$user->lastname' required>
            </div>
          </div>
          <div class='rowTab'>
            <div class='labels'>
              <label for='firstname' class='label'>Prénom*</label>
            </div>
            <div class='rightTab'>
              <input type='text' name='firstname' placeholder='Entrez votre prénom' class='input-field' value='$user->firstname' required>
            </div>
          </div>
          <div class='rowTab'>
            <div class='labels'>
              <label for='password' class='label'>Mot de passe* (6 caractères minimum)</label>
            </div>
            <div class='rightTab'>
              <input type='password' pattern='.{6,}' required title='6 caractères minimum' name='pass1' placeholder='Entrez un mot de passe' class='input-field' required>
            </div>
          </div>
          <div class='rowTab'>
            <div class='labels'>
              <label for='pass2' class='label'>Confirmation du nouveau mot de passe</label>
            </div>
            <div class='rightTab'>
              <input type='password' name='pass2' placeholder='Confirmez mot de passe' class='input-field' required>
            </div>
          </div>
          <div class='bton'>
            <input class='btn btn-secondary' type='submit' value='Modifier'>
            <a href='/mywishlist/userHome' class='btn btn-danger'>Annuler</a>
          </div>
        </form>
        
      </div>
      </div>
  </div>";
  }

  public function linkListe()
  {

    return "
    <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id ='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'> <img src='https://img.icons8.com/cotton/64/000000/wedding-gift.png' alt= 'wishlist'  height = '30' class ='giftbox'> MyWishList</a>
      </li>
      <li class='nav-item'><a class='nav-link' href='/mywishlist'>Home</a></li>
      <li class='nav-item'><a class='nav-link' href='/logout'>Deconnexion</a></li>
    </ul>
  </header>
  <!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id= 'jumbotronConnex'>
    <h1 class='text-center'>Modifier un compte</h1>
  </div>
  <!--FIN JUMBOTRON-->
  </div>
  <div class='wrapper'>
    <div class='row'>
      <div class='col-sm-3 ' id='leftside'>
        <h3 class='text-center'>Comment faire ?</h3>
        <p>
          Pour lier une liste à votre compte, rien de plus simple, entrez le token donnée lors de la création de la liste si vous en êtes le créateur, ou celui donné par vos amis. 
        </p>
      </div>
      <div class='col-sm-7'>
        <!-- zone de connexion -->
        <form action='./userLinkListe' method='POST'>
          <div class='rowTab'>
            <div class='labels'>
              <label for='lastname' class='label'>Token*</label>
            </div>
            <div class='rightTab'>
              <input type='text' name='token' placeholder='Entrez le token' class='input-field' required>
            </div>
          </div>  
          <div class='bton'>
            <input class='btn btn-success' type='submit' value='Ajouter'>
            <a href='/mywishlist/userHome' class='btn btn-danger'>Annuler</a>
          </div>
        </form>
        
      </div>
      </div>
  </div>";
  }

  public function getParticipation()
  {

    $u = $_SESSION['currentUser'];

    echo " <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
     <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/logout'>Déconnexion</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Mes participations</h1>
  </div><!--FIN JUMBOTRON-->";
    echo "<div class='text-center'>
            <h1>Voici les items auxquels vous participez</h1>
          </div>";
    $particpation = $u->itemParticipate()->get();

    if (count($particpation) ===0 ) {
      echo "<div class='text-center'>
              Il n'y a pas de participation
            </div>";
    } else {
      foreach ($particpation as $pa) {
        echo <<<EOF
                <div class='text-center'>
                    <p>
                      <strong>nom:</strong> $pa->nom 
                    </p>
                    <p>
                      <strong>Prix:</strong> $pa->tarif €
                    </p>
                    <hr>
                </div>
EOF;
      }
    }



    echo "<a href='/mywishlist/userHome' class='btn btn-info' >retour</a>";
  }
}
