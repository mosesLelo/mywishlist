<?php
namespace Vues;

use Models\Liste;

class createListView
{

  protected $actionToShow;


  public function __construct($actionToShow)
  {
    $this->actionToShow = $actionToShow;
  }

  public function render()
  {

    $elementToRender = "";

    switch ($this->actionToShow) {

      case 'creerList':
        $elementToRender = $this->createList();
        break;
      case 'editList':
        $elementToRender = $this->editList();
        break;
      case 'getAllList':
        $elementToRender = $this->getAllList();
        break;
      case 'showList':
        $this->showUserList();
        break;
      case 'editList':
        $elementToRender->editList();
        break;
      case 'listeDetail':
        $this->listeDetails();
        break;
      case 'listeDetailsParticipate':
        $this->listeDetailNotCreator();
        break;
      case 'publicList':
        $this->publicList();
        break;
    }

    echo <<<EOF
       <!DOCTYPE html>
<html lang="fr">
<!-- HEAD -->

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link href="/mywishlist/src/web/style.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <a href="https://icons8.com/icon/53787/wedding-gift"></a>
  <title>WishList</title>
</head>
<!-- FIN HEAD -->

<body>
  <!-- BODY-->
  $elementToRender
  <!-- element à renvoyer -->
  <footer class="pagefooter">
    <!-- FOOTER -->
    <div class="myquote">
      <blockquote>
        <!-- avis-->
        <span class="quote">“</span> J'adore ce concept, il est clair et simple à utiliser. A chaque fois que je dois faire une liste de cadeaux ou une cagnotte, j'utilise myWishlist, cela me facilite la vie!
        <br>
        <em>Kévin, utilisateur depuis 8 mois</em>
      </blockquote>
    </div> <!-- FIN DIV myquote-->
    <!-- fin  -->
    <div class="menu">
      <div class="row">
        <div class="col-md-12">
          <h3 class="text-center">MyWishlist</h3>
          <img src="https://img.icons8.com/cotton/64/000000/wedding-gift.png" class="gitfbox" alt='wishlist'>
          </div>
      </div><!-- FIN DIV row-->
    </div><!-- FIN DIV Menu-->
    <div class="footer-bottom text-center">
      <div class = "Mywish">
        <p><span>© 2019,MyWishlist, Privacy Policy,Terms & Conditions        
          </span>
          </p>
      </div><!-- FIN DIV Mywish-->
    </div> <!-- FIN DIV FOOTER BOTTOM-->
  </footer>
   <script>
window.addEventListener('scroll', function (e) {
        var mynav = document.getElementById('mynav');
        if (document.documentElement.scrollTop || document.body.scrollTop > window.innerHeight) {
                mynav.classList.add('nav-colored1');
                mynav.classList.remove('nav-transparent1');
            } else {
                mynav.classList.add('nav-transparent1');
                mynav.classList.remove('nav-colored1');
            }
    });
    </script>
</body>
<!-- FIN BODY-->
</html><!-- FIN HTML-->
EOF;
  }

  /* Fonction pour générer page pour création de liste*/


  //todo ajouter lien page accueil et moncompte
  public function createList()
  {
    if(isset($_SESSION['user'])){
      $userAction="
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/userListe'>Mes listes</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
      </li>
      <li class='nav-item'>
      <a class='nav-link' href='./logout'>Deconnexion</a>
      </li>";
    }else {
      $userAction="<li class='nav-item'>
      <a class='nav-link' href='/mywishlist/register'>Inscription</a>
      </li>
       <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/login'>Connexion</a>
      </li>";
    }

    return "
         <header>
  <!-- NAVBAR -->
  <ul class='nav nav-pills fixed-top'id ='mynav'>
    <li class='nav-item'>
      <a class='navbar-brand' href='#'> <img src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'  height = '30' class ='giftbox'alt= 'wishlist'> MyWishList</a>
    </li>
    <li class='nav-item'>
    <a class='nav-link' href='/mywishlist'>Accueil</a>
    </li>
    <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
    $userAction
    <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
  </ul>
</header>
<!-- jumbotron -->
<div class='jumbotron jumbotron-fluid' id= 'jumbotronListe'>
  <h1 class='text-center'>Créer une liste</h1>
</div>
<!--FIN JUMBOTRON-->
</div>
<div class='wrapper'>
  <div class='row'>
    <div class='col-sm-3' id='leftside'>
      <h3 class='text-center'>Comment faire ?</h3>
      <p>
        Entrez vos données, elles permettront à vos amis et à votre famille de retrouver votre liste sur notre site.Vous pouvez modifier ou supprimer votre compte et vos listes à tout moment. Si vous souhaitez plus de renseignements sur le concept MyWishlist,
        rendez-vous sur la
        <a href='/mywishlist' class='lien'>page d'accueil</a>
      </p>
    </div>
    <div class='col-sm-7'>
      <form id='survey-form' method='POST' action='createListe'>
        <div class='rowTab'>
          <div class='labels'>
            <label class='name-label2' for='name'>*Nom de liste: </label>
          </div>
          <div class='rightTab'>
            <input autofocus type='text' name='name' id='name' class='input-field' placeholder='Entrez le nom de la liste' required>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='comments'>Description de la liste:</label>
          </div>
          <div class='rightTab'>
            <textarea id='comments' class='input-field' style='height:50px;resize:vertical;' name='comment' placeholder='Entrez votre description ici.'></textarea>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label id='name-label3' for='name'>*Date d'expiration </label>
          </div>
          <div class='rightTab'>
            <input type='date' name='Date_expiration:' class='input-field' required>
          </div>
        </div>
        <div class='bton'>
          <button id='submit' type='submit'>Créer</button>
        </div>
      </form>
    </div>
  </div>
</div>";
  }

  public function editList()
  {

    $li = $_SESSION['listEdit'];

    if(isset($_SESSION['user'])){
      $userAction = "<li class='nav-item'>
      <a class='nav-link' href='./logout'>Deconnexion</a>
      </li>";
    }else {
      $userAction ="";
    }

    return "
      <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
       <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/userListe'>Mes listes</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      $userAction
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>

  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Editer ma liste</h1>
  </div><!--FIN JUMBOTRON-->
  <div class='wrapper'>
    <div class='row'>
      <div class='col-sm-3' id='leftside'>
        <h3 class='text-center'>Comment faire ?</h3>
        <p>Editer une liste, rien de plus simple , modifiez les informations désirées et valider votre changement, si vous souhaitez visualiser l'ensemble de vos listes rendez-vous <a href='' class='lien'>ici</a></p>
      </div>
      <div class='col-sm-7'>
        <form action='../editListe/$li->edit_token' method='post'>
          <div class='rowTab'>
            <div class='labels'>
              <label for='title'>Titre:</label>
            </div>
            <div class='rightTab'>
              <input name='title' type='text' value='$li->titre'>
            </div>
          </div>
          <div class='rowTab'>
            <div class='labels'>
              <label>Description:</label>
            </div>
            <div class='rightTab'>
              <textarea cols='30' name='descr' rows='10'>$li->description
              </textarea>
            </div>
          </div>
          <div class='rowTab'>
            <div class='labels'>
              <label>Expiration:</label>
            </div>
            <div class='rightTab'>
              <input name='date' type='date' value='$li->expiration'>
            </div>
          </div>
          <div class='rowTab'>
            <div class='labels'>
              <label>Partager Liste:</label>
            </div>
            <div class='rightTab'>
              <select name='shared'>
                <option value='1'>
                  oui
                </option>
                <option value='0'>
                  non
                </option>
              </select>
            </div>
          </div><button id='submit' type='submit'>Editer</button>
        </form>
      </div>
    </div>
  </div>
          ";
  }



  public function showUserList()


  {
    echo "
     <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
     <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='./logout'>Deconnexion</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Mes listes</h1>
  </div><!--FIN JUMBOTRON-->
  <div class = container>";

    $listes = $_SESSION['userListes'];

    foreach ($listes as $liste) {

      if ($liste->shared == 1) {
        $partage = "<strong>public</strong>";
      } else {
        $partage = "<strong>Non public</strong>";
      }


      echo " 
      <div class = 'row titreliste'>
       <p class = 'titre'><a  class = 'text-center'  href='listeDetails/$liste->edit_token' ><strong>Titre:</strong> $liste->titre</a></p> 
      </div>
      <div class = 'row '>
      <div class = 'col-md-6 meslistes'>
         <p class = 'text-justify'><strong>Description: </strong>$liste->description</p>
        </div>
        <div class = 'col-md-6 meslistes' >
        <p><a class = 'btn btn-info'href=' editListe/$liste->edit_token'  id = 'submit' role = 'button'>Editer liste</a></p>
        <p><a class = 'btn btn-info'href='addItemToListe/$liste->edit_token'  id = 'submit'role = 'button'>++ Item</a></p>
        </div>
        </div>
        <div class = 'row'>
          <div class='col-md-6'>
        <p class = 'listepub'><strong>Date de fin :</strong> $liste->expiration</p>
      </div>
      <div class = 'col-md-6'>
         <p class = 'listepub'>Liste publique : $partage </p>
       </div>
       </div>
        <hr class = 'list'> ";
    }
    echo "</div>";
  }


  public function listeDetails()
  {

    $l =  $_SESSION['listeDetails'];
    $token = $_SESSION['currentToken'];
    $partage = $_SESSION['tokenPartage'];
    $currentDate = date('Y-m-d');
    $listeDate = date($l->expiration);
    if(isset($_SESSION['user'])){

      $userAction ="<li class='nav-item'>
      <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
       </li>
       <li class='nav-item'>

         <a class='nav-link' href='/mywishlist/logout'>Déconnexion</a>
       </li>";
    }else{
      $userAction="<li class='nav-item'>
      <a class='nav-link' href='/mywishlist/register'>Inscription</a>
      </li>
       <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/login'>Connexion</a>
      </li>";
    }

    echo " <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      $userAction
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Ma liste détaillée</h1>
  </div><!--FIN JUMBOTRON-->";
    //Il faut des boutons pour que l'utilisateur non connecté puisse ajouter des elements à sa liste
    echo "
     <div class='row justify-content-around introduction'>
     <div class='col-md-8 Synop'>
     <div class = 'mntitre'>
     <h1 class = 'text-center'>$l->titre</h1>
     <br>
     </div>
     <div class = 'text-center'>
     <img src='https://img.icons8.com/clouds/100/000000/edit.png'>
               <a href='../editListe/$token'class='btn btn-success'> Modifier la liste</a>  
                <img src='https://img.icons8.com/clouds/100/000000/plus.png'>
               <a href='../addItemToListe/$token'class='btn btn-success'>Ajouter Item</a>
               <img src='https://img.icons8.com/clouds/100/000000/close-window.png'>  
               <a href='../deleteListe/$token'class='btn btn-danger' >Supprimer Liste </a>
              </div> 
          </div>
          
          <div class = 'facts col-md-3'>
          <h2>Tokens</h2>
          <ul>
          <li class = 'text-center'>Token de partage : $partage</li><br>
          <li class = 'text-center'>Token pour lier la liste au compte : $token</li>
          </ul>
          </div>
          </div>
          <hr>";
    //Cette partie affiche les items liés à la liste
    $items = $l->items()->get();

    foreach ($items as $item) {
      //affiche si une cagnotte est déjà créée et desactive le boutton de la cagnotte
      if ($item->cagnotted == 1) {
        $hasCagnotte = "<strong>Cagnotte</strong>";
        $buttonCagnotte = "";
      } else {
        $hasCagnotte = "<p><strong>Pas de cagnotte</strong></p>";
        $buttonCagnotte = " <a href='../createCagnotte/$token/$item->id' class='btn btn-primary-bouton'>Créer cagnotte </a>";
      }

      //verifie si l'item est reservé
      if ($item->participation_name === null) {
        $hasReservation = "<p><strong>Article non reservé</strong></p>";
        $buttonAddDelete = "<a href='../userEditItem/$token/$item->id' class='btn btn-primary-bouton' >Modifier</a> 
          <a href='../deleteItem/$token/$item->id' class='btn btn-danger'>Supprimer Item</a> ";
      } else {
        $hasReservation = "<p><strong>Article reservé</strong></p>";
        $buttonAddDelete = "";
      }

      /**
       * Si la liste est expirée afficher le nom et le message du participant
       */
      if ($listeDate <= $currentDate) {
        $showCommentItem = "<p>Participant : $item->participation_name</p>
        <p>Message participant : $item->participation_msg</p>
        <hr>";
      } else {
        $showCommentItem = "";
      }


      if ($item->img !== NULL) {
        $showImg = "../src/web/img/$item->img";
      } else {
        $showImg = $item->url;
      }

      echo <<<EOF

        <div class='row text-center introduction'>
     <div class='col-md-8 Synop'>
     <div class = 'mntitre'>
      <h3 class = "text-center">Nom : $item->nom</h3>
      <br>
  </div>    
      <img src="$showImg" width =30%  height =30% alt="No Images">
      <br> <br>
      </div>

      <div class = 'fact col-md-3'>     
          <ul>
          <li class = 'text-center'>Tarif : $item->tarif €</li><br>
          <li class = 'text-center'>$hasCagnotte</li>
          <li class = 'text-center'>$hasReservation</li>
          
          </ul>
          </div>
           <div class = 'montruc'>
          <p>$showCommentItem</p>
          <p>$buttonAddDelete $buttonCagnotte</p>
          </div>
          </div>
        </div>
          <hr>

     

      


      
EOF;
    }

    //Cette partie du code permet d'afficher les commentaires

    echo "<h2 class = 'text-center'>Commentaires:</h2>";

    $comments = $l->comments()->get();
    if (count($comments) == 0) {
      echo "<p>Pas de commentaires</p> ";
    } else {

      foreach ($comments as $comment) {
        echo <<<EOF
          <p>Nom : $comment->username</p>
 
          <p>Commentaire : $comment->comment</p>
      
EOF;
      }
    }
    echo "<p><a class= 'btn btn-success-bouton' href='/mywishlist/userListe' role = 'button' >Retour</a></p>";
  }



  /**
   * Se charge d'afficher la liste pour les participants
   */
  public function listeDetailNotCreator()
  {

    $l =  $_SESSION['listeDetails'];
    $token = $_SESSION['currentToken'];


    $liste = Liste::where('token', 'LIKE', $token)->first();
    $userAction = "";
    $name = "";

    if (isset($_SESSION['user'])) {
      $name = $_SESSION['user']['firstname'];
    }
    echo " <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
       </li>
       <li class='nav-item'>
         <a class='nav-link' href='./logout'>Déconnexion</a>
       </li>
       <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Participez à la Liste</h1>
  </div><!--FIN JUMBOTRON-->";

    echo "<div class = 'text-center' >
            <h1> Participation à la liste $liste->titre </h1>
          </div>";

    //Cette partie affiche les items liés à la liste
    $items = $l->items()->get();
    foreach ($items as $item) {
      /**
       * Gestion participation item
       */
      //regarde si un utilisateur n'as pas déjà participé
      if ($item->participation_name === null) {
        $canParticipateItem = "<p><a href='../participate/$token/$item->id' class='btn btn-primary'>Participer item</a></p>";
      } else {
        $canParticipateItem = "";
      }


      //affiche si une cagnotte est déjà créée et desactive le bouton de la cagnotte
      if ($item->cagnotted == 1) {
        $hasCagnotte = "<p><strong>cagnotte</strong></p>";
        $canParticipateCagnotte = "<p><a href='../participateCagnotte/$token/$item->id' class='btn btn-primary'>Participé cagnotte</a></p>";
      } else {
        $hasCagnotte = "<p><strong>Pas de cagnotte</strong></p>";
        $canParticipateCagnotte = "";
      }

      $userAction = " <p>Participant : $item->participation_name</p>
      <p>Message participant : $item->participation_msg</p>
  
      <p>$canParticipateItem</p>

      <p>$canParticipateCagnotte</p>
    ";

      //Verifie se l'image est lien ou non


      if ($item->img !== NULL) {
        $showImg = "../src/web/img/$item->img";
      } else {
        $showImg = $item->url;
      }

      echo <<<EOF

      <p>Nom : $item->nom;</p>
      <img src="$showImg" width =30%  height= 30% alt="../src/web/img/noImage.png">
      <p>Tarif : $item->tarif €</p>
      <p>$hasCagnotte</p>
      <p>$userAction</p>
      
EOF;
    }

    //Cette partie du code permet d'afficher les commentaires

    echo "<h2>Commentaires:</h2>
    <form action='../listeAddComment/$token' method='post'>
    <label for='nom'>Nom:</label>
    <input type='text' name='nom' value='$name'>
    <label for='comment'>Commentaire</label>
    <textarea name='comment' cols='10' rows='10'></textarea>
  
    <input type='submit'>
    </form>";

    $comments = $l->comments()->get();

    if (count($comments) == 0) {
      echo " <p> Commentaires</p>";
    } else {

      foreach ($comments as $comment) {
        echo <<<EOF
          <p>Nom : $comment->username</p>
          <p>Commentaire : $comment->comment</p>
       
EOF;
      }
    }

    echo "<p><a class = 'btn btn-info 'href='/mywishlist role = 'button'>retour</a></p>";
  }


  public function publicList()
  {
    if(isset($_SESSION['user'])){
      $userAction = " <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
       </li> 
       <li class='nav-item'>
         <a class='nav-link' href='./logout'>Deconnexion</a>
       </li>";
    }else {
      $userAction="";
    }

    echo "
     <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
          $userAction
          <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Mes listes publiques</h1>
  </div><!--FIN JUMBOTRON-->
  <div class = container>";

    $listes = $_SESSION['publicListes'];


    if (count($listes) === 0) {
      echo "<div class = 'text-center'>
              <h1> Il n'y a pas de listes publiques </h1>
            </div>";
    } else {
      foreach ($listes as $liste) {
        
        if(isset($_SESSION['user'])){
          $participate ="<a  class = 'text-center'  href='listeDetailsParticipate/$liste->token' ><strong>Titre:</strong> $liste->titre</a>" ;
        }else {
          $participate ="<div class = 'text-center'><p><strong>Titre:</strong> $liste->titre</p> </div>";
        }
        echo " 
        <div class = 'row titre'>
        $participate
        </div>
        <div class = 'row'>
          <div class = 'col-md-12 meslistes'>
            <p class = 'text-justify'><strong>Description: </strong>$liste->description</p>
          </div>
          </div>
          <div class = 'row'>
            <div class='col-md-12'>
          <p class = 'listepub'><strong>Date de fin :</strong> $liste->expiration</p>
          </div>
         </div>
          <hr class = 'list'> ";
      }
    }
    echo "</div>";
  }
}
