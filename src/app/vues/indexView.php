<?php
namespace Vues;

class indexView {

    protected $actionToShow;

    public function __construct($actionToShow){
       $this->actionToShow = $actionToShow;
    }

    public function render(){

        $elementToRender = "UNDER CONSTRUCTION mon ami !!!";

        switch ($this->actionToShow) {

            
            case 'guest':
                $elementToRender = $this->homeGuests();
            
        }

        echo <<<EOF
      <!DOCTYPE html>
<html lang="fr">
<!-- HEAD -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <link href="./src/web/style.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <title>WishList</title>
</head>
<body>
  <a href="https://icons8.com/icon/53787/wedding-gift"></a> <!-- FIN HEAD -->
   <!-- BODY-->
   $elementToRender <!-- element à renvoyer -->
  <footer class="pagefooter">
    <!-- FOOTER -->
    <div class="myquote">
      <blockquote>
        <!-- avis  -->
        <span class="quote">“</span> My Wishlist est très simple d'utilisation et vous permet de créer et gérer vos listes en un clin d'oeil, un incontournable pour les anniversaires, mariages et naissances.<br>
        <em>Elvina, utilisatrice depuis 3 ans</em>
      </blockquote>
    </div><!-- fin  -->
    <div class="menu">
     <div class="row">
        <div class="col-md-12">
          <h3 class="text-center">MyWishlist</h3>
          <img src="https://img.icons8.com/cotton/64/000000/wedding-gift.png" class="gitfbox" alt='wishlist'>
          </div>
      </div><!-- FIN DIV row-->
    </div><!-- FIN DIV Menu-->
    <div class="footer-bottom text-center">
      <div class = "Mywish">
        <p><span>© 2019,MyWishlist, Privacy Policy,Terms & Conditions        
          </span>
          </p>
      </div><!-- FIN DIV Mywish-->
    </div> <!-- FIN DIV FOOTER BOTTOM-->
  </footer>
  <script>
window.addEventListener('scroll', function (e) {
        var mynav = document.getElementById('mynav');
        if (document.documentElement.scrollTop || document.body.scrollTop > window.innerHeight) {
                mynav.classList.add('nav-colored');
                mynav.classList.remove('nav-transparent');
            } else {
                mynav.classList.add('nav-transparent');
                mynav.classList.remove('nav-colored');
            }
    });
    </script>
</body>
<!-- FIN BODY-->
</html><!-- FIN HTML-->         
EOF;
        
    }

/* Fonction pour page accueil des utilisateurs connectés 
    -Définir les fonctionnalités pour connectés. 

*/
  

     public function homeguests(){

         return"
 <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top ' id ='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='login'>Connexion</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='register'>Inscription</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id = 'jumbotronHome'>
    <h1 class='text-center'>MyWishlist</h1>
  </div><!--FIN JUMBOTRON-->
  <div class='section1'>
    <div class='title'>
      <h1 class='text-center'>Pourquoi choisir myWishList?</h1><br>
      <br>
    </div>
    <div class='card-deck'>
      <div class='card text-center' id= 'boite'>
        <img class='icon' height='40' src='https://img.icons8.com/color/%2048/000000/facebook-like.png' width='40'>
        <div class='card-body'>
          <h5 class='card-title text-center'>100% gratuit</h5>
          <p>Toutes les fonctionnalités sans frais, pas de frais cachés!</p>
        </div>
      </div>
      <div class='card text-center' id= 'boite1'>
        <img class='icon' height='40' src='https://img.icons8.com/color/48/000000/purse.png' width='40'>
        <div class='card-body'>
          <h5 class='card-title text-center'>Cagnotte</h5>
          <p>Créez votre cagnotte en ligne en quelques clics seulement!</p>
        </div>
      </div>
      <div class='card text-center' id= 'boite2'>
        <img class='icon' height='40' src='https://img.icons8.com/color/48/000000/trust.png' width='40'>
        <div class='card-body'>
          <h5 class='card-title text-center'>9,5/10</h5>
          <p>Note de satisfaction de nos utilisateurs</p>
        </div>
      </div>
      <div class='card text-center' id= 'boite3'>
        <img class='icon' height='40' src='https://img.icons8.com/color/48/000000/smartphone-tablet.png' width='40'>
        <div class='card-body'>
          <h5 class='card-title text-center'>Mobiles et tablettes</h5>
          <p>Tous nos outils fonctionnent parfaitement sur mobiles et tablettes</p>
        </div>
      </div>
    </div><br>
    <hr>
    <br>
    <div class='wrapper'>
      <h2 class='text-center'>Commencez dès maintenant</h2>
      <div class='row'>
        <!-- row -->
        <div class='col-lg-4'>
          <img src='https://img.icons8.com/color/96/000000/login-as-user.png'width='150'>
          <p><a class='btn btn-info' href='login'  id = 'submit'role='button'>Connexion</a></p>
        </div>
        <div class='col-lg-4'>
          <!--  col -->
          <img src='https://img.icons8.com/dusk/64/000000/membership-card.png'width ='150'>
          <p><a class='btn btn-info' href='register' id = 'submit'role='button'>S'inscrire</a> </p>
        </div><!-- fin col -->
        <div class='col-lg-4'>
          <!--  col -->
          <img src='https://img.icons8.com/clouds/100/000000/todo-list.png'width ='150'>
          <p><a class='btn btn-info' href='./createListe' role='button' id = 'submit'>Créer  liste</a></p>
        </div><!-- fin col -->
      </div><!-- fin  row -->
    </div><!-- fin  wrapper -->
  </div>";


        
    }



}
?>