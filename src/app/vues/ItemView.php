<?php
namespace Vues;

class itemView {

    protected $actionToShow;

    public function __construct($actionToShow){
       $this->actionToShow = $actionToShow;
    }

    public function render(){

        $elementToRender = "";

        switch ($this->actionToShow) {            
            case 'createItem':
              $elementToRender  = $this->createItem();
            break;
            case 'editItem':
              $elementToRender  = $this->editItem();
            break;
            case 'participateItem':
              $elementToRender = $this->participateItem();
              break;
        }

        echo <<<EOF
      <!DOCTYPE html>
<html lang="fr">
<!-- HEAD -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <link href="/mywishlist/src/web/style.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <title>WishList</title>
</head>
<body>
  <a href="https://icons8.com/icon/53787/wedding-gift"></a> <!-- FIN HEAD -->
   <!-- BODY-->
   $elementToRender <!-- element à renvoyer -->
  <footer class="pagefooter">
    <!-- FOOTER -->
    <div class="myquote">
      <blockquote>
        <!-- avis  -->
        <span class="quote">“</span> My Wishlist est très simple d'utilisation et vous permet de créer et gérer vos listes en un clin d'oeil, un incontournable pour les anniversaires, mariages et naissances.<br>
        <em>Elvina, utilisatrice depuis 3 ans</em>
      </blockquote>
    </div><!-- fin  -->
    <div class="menu">
     <div class="row">
        <div class="col-md-12">
          <h3 class="text-center">MyWishlist</h3>
          <img src="https://img.icons8.com/cotton/64/000000/wedding-gift.png" class="gitfbox" alt='wishlist'>
          </div>
      </div><!-- FIN DIV row-->
    </div><!-- FIN DIV Menu-->
    <div class="footer-bottom text-center">
      <div class = "Mywish">
        <p><span>© 2019,MyWishlist, Privacy Policy,Terms & Conditions        
          </span>
          </p>
      </div><!-- FIN DIV Mywish-->
    </div> <!-- FIN DIV FOOTER BOTTOM-->
  </footer>
  <script>
window.addEventListener('scroll', function (e) {
        var mynav = document.getElementById('mynav');
        if (document.documentElement.scrollTop || document.body.scrollTop > window.innerHeight) {
                mynav.classList.add('nav-colored');
                mynav.classList.remove('nav-transparent');
            } else {
                mynav.classList.add('nav-transparent');
                mynav.classList.remove('nav-colored');
            }
    });
    </script>
</body>
<!-- FIN BODY-->
</html><!-- FIN HTML-->         
EOF;
        
    }


    public function createItem(){

        $id = $_SESSION['id'];
        $token= $_SESSION['token'];

      return "
       <header>
    <!-- NAVBAR -->
    <ul class='nav nav-pills fixed-top' id='mynav'>
      <li class='nav-item'>
        <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist'>Accueil</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/userListe'>Mes listes</a>
      </li>
      <li class='nav-item'>
    <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
    </ul>
  </header><!-- jumbotron -->
  <div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
    <h1 class='text-center'>Ajouter un article à une liste</h1>
  </div><!--FIN JUMBOTRON-->

  <div class='wrapper'>
  <div class='row'>
    <div class='col-sm-3' id='leftside'>
      <h3 class='text-center'>Comment faire ?</h3>.
      <p>
        Complétez les champs demandés et validez votre choix , l'article sera alors ajouté à la liste, n'oubliez pas de donnez un maximum d'informations sur l'article pour aider les participants à faire leur choix
      </p>
    </div>
    <div class='col-sm-7'>
      <form action='../../newItem/$token/$id' method='post'>
    
      <div class='rowTab'>
          <div class='labels'>
      <label for='nom'>Nom*:</label>
       </div>
          <div class='rightTab'>
      <input type='text' name='nom' required>
       </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
      <label for'descr'>Description: </label>
       </div>
       <div class='rightTab'>
      <textarea name='descr' id='' cols='30' rows='10'></textarea>
       </div>
        </div>
   <div class='rowTab'>
          <div class='labels'>
      <label for='price'>Prix*:</label>
       </div>
       <div class='rightTab'>
      <input type='text' name='price'required >
      </div>
            </div>
       <div class='rowTab'>
          <div class='labels'>
      <label for='image'>Image Url:</label>
      </div>
       <div class='rightTab'>
      <input type='text' name='image'>
            </div>
                  </div>
                   <div class='bton'>
     <button id='submit' type='submit'>Créer</button>
       </div>
      </form>
    </div>
  </div>
</div>";
    }

     public function editItem(){

      $id =$_SESSION['id'];
      $token =$_SESSION['token'];
      $item =$_SESSION['item'];

      if(isset($_SESSION['user'])){
        $showUpload ="<div class='rowTab'>
        <div class='labels'>
          <label for='image'>Upload Image:</label>
          <button> <a href='../../upload/$token/$id'>uploader</a></button>
        </div>
      </div>";
      }
      else{
        $showUpload="";
      }

      return " 
<header>
  <!-- NAVBAR -->
  <ul class='nav nav-pills fixed-top' id='mynav'>
    <li class='nav-item'>
      <a class='navbar-brand' href='#'><img alt='wishlist' class='giftbox' height='30' src='https://img.icons8.com/cotton/64/000000/wedding-gift.png'> MyWishList</a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='/mywishlist'>Accueil</a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='mywishlist/userListe'>Mes listes</a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/userHome'>Mon compte</a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='/mywishlist/createListe'>Créer une liste</a>
    </li>
    <li class='nav-item'>
        <a class='nav-link' href='/mywishlist/public'>Listes publiques</a>
      </li>
  </ul>
</header>
<!-- jumbotron -->
<div class='jumbotron jumbotron-fluid' id='jumbotronListe'>
  <h1 class='text-center'>Editer un article</h1>
</div>
<!--FIN JUMBOTRON-->
<div class='wrapper'>
  <div class='row'>
    <div class='col-sm-3' id='leftside'>
      <h3 class='text-center'>Comment faire ?</h3>
      <p>Editer votre article en modifiant les informations principales, vous pouvez également changer ou charger une image si vous le souhaitez en appyant sur le bouton télécharger.></p>
    </div>
    <div class='col-sm-7'>
      <form action='../../userEditItem/$token/$id' method='post'>
        <div class='rowTab'>
          <div class='labels'>
            <label for='nom'>Nom:</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='nom' value='$item->nom'>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for 'descr'>Description: </label>
          </div>
          <div class='rightTab'>
            <textarea name='descr' id='' cols='30' rows='10'>$item->descr</textarea>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='price'>Prix:</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='price' value='$item->tarif'>
          </div>
        </div>
        <div class='rowTab'>
          <div class='labels'>
            <label for='image Url'>Image:</label>
          </div>
          <div class='rightTab'>
            <input type='text' name='image' value='$item->url'>
          </div>
        </div>
        $showUpload
        <button id='submit' type='submit'>Editer</button>
      </form>
      <button><a href='../../listeDetails/$token'>Annuler</a></button>
    </div>
  </div>
</div>";
     }


     public function participateItem(){

      $id =$_SESSION['id'];
      $name =$_SESSION['name'];
      $token =$_SESSION['token'];

       return "<form action='../../participate/$token/$id' method='post'>
       <p>
       <label for='nom'>Nom participant:</label>
       <input type='text' name='nom' value='$name'>
       </p>
       <p>
       <label for'msg'>Message:</label>
       <textarea name='msg' id='' cols='30' rows='10'></textarea>
       </p>
       <input type='submit'>
       </p>
       </form>
       <br/>" ;
     }

}
