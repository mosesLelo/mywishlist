<?php
namespace Models;

class Liste extends \Illuminate\Database\Eloquent\Model
{
  protected $table = 'liste';
  protected $primaryKey = 'no' ;
  public $timestamps = false ;

  public function items()
  {
      return $this->hasMany('Models\Item','liste_id');
  }

  public function users()
  {
      return $this->belongsTo('Models\User', 'user_id');
  }

  //Tous les commentaires attachés à une liste
  public function comments()
  {
      return $this->hasMany('Models\ListeComment','list_id');
  }
}

?>
