<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

class User extends Model{

    protected $table ='user';
    protected $primaryKey = 'user_id' ;
    public $timestamps = false ;


    public function listes()
    {
        return $this->hasMany('Models\Liste','user_id');
    }

    //Un utilisateur peut participer sur plusieur item
    public function itemParticipate()
    {
        return $this->hasMany('Models\Item','participation_user');
    }

    //Un utilisateur peut participer à une cagnotte
    public function items()
    {
        return $this->belongsTo('Models\Cagnotte', 'user_id');
    }

}
?>