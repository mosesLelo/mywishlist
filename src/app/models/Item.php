<?php
namespace Models;

class Item extends \Illuminate\Database\Eloquent\Model
{
  protected $table = 'item';
  protected $primaryKey = 'id' ;
  public $timestamps = false ;

  public function listes()
  {
      return $this->belongsTo('Models\Liste', 'liste_id');
  }


  //Un item peut avoir un utilisateur participant
  public function participationUser()
  {
      return $this->belongsTo('Models\User', 'participation_user');
  }

  //Un item peut avoir beaucoup de cagnotte
  public function cagnottes()
  {
      return $this->hasMany('Models\Cagnotte','item_id');
  }

}
