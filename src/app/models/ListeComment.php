<?php
namespace Models;

class ListeComment extends \Illuminate\Database\Eloquent\Model
{
  protected $table = 'comment';
  protected $primaryKey = 'id' ;
  public $timestamps = false ;

  //Un commentaire appartient à une liste
  public function listes()
  {
    return $this->belongsTo('Models\Liste', 'liste_id');
  }
}

?>
