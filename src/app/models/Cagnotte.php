<?php
namespace Models;

class Cagnotte extends \Illuminate\Database\Eloquent\Model
{
  protected $table = 'cagnotte';
  protected $primaryKey = 'cagnotte_id' ;
  public $timestamps = false ;

  //Une cagnotte appartient à un item
  public function items()
  {
    return $this->belongsTo('Models\Item', 'item_id');
  }

  //Une cagnotte peut avoir un utilisateur particitant
  public function users()
  {
    return $this->hasMany('Models\User', 'user_id');
  }

}

?>
