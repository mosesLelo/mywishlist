<?php

/**
 * Cette classe a pour but de gerer les utilisateurs
 * 
 */

namespace Controllers;

use  Models\User;
use Vues\userView;
use Models\Item;
use Models\Liste;

class UserController
{

	public $app;

	/*constructeur*/

	public function __construct($app)
	{
		$this->app = $app;
	}

	//Fonction verifiant si l'utilisateur est unique
	public function isUserUnique($userToRegister)
	{
		//Parcours l'ensemble des utilisateurs dans une base de données
		$users = User::select('login')->get();
		foreach ($users as $user) {
			if ($user->login === $userToRegister) {
				return false;
			}
		}

		return true;
	}
	//Fonction permettant d'eviter les injections sql 
     function StringInputCleaner($data){
        //Enleve l'espace avant et après
        $data = trim($data); 
        //enlève les slashes
        $data = stripslashes($data); 
        $data=(filter_var($data, FILTER_SANITIZE_STRING));
        return $data;
    }	

	/**
	 * Fonction qui renvoie a la page perso de
	 * l'utilsateur
	 */
	public function userHome()
	{

		$this->app->get('/userHome', function () {
			$view = new userView("moncompte");
			$view->render();
		})->name('moncompte');
	}



	/* fonction vue pour l'inscription  */
	/*voir pour ajouter restriction sur le choix du mot de passe
	 - min 8 
	  - caractères */
	/* vérifier l'unicité du login */
	public function registration()
	{

		$this->app->get('/register', function () {
			$view = new userView("inscription");
			$view->render();
		})->name('register');
	}

	//Todo :Verifier que le login est unnique
	public function processRegistration()
	{
		$this->app->post('/register', function () {
			$app = \Slim\Slim::getInstance();
			$data = $app->request->post();

			if (!empty($data['login']) && !empty($data['pass1']) && !empty($data['pass2'])) {

				//Verifie si l'utilisateur est unique
				$unique = $this->isUserUnique($data['login']);

				//Verifie que la les deux passes sont egaux et que le mdp soit sup à 6 
				if ($data['pass1'] === $data['pass2']  && strlen($data['pass1']) >= 6 && $unique) {
					//On hash le mot de passe
					$pass = password_hash($data['pass1'], PASSWORD_DEFAULT);

					//Insertion des informations dans la base de donnée    
					$user = new User();
					$user->login = $data['login'];
					$user->password = $pass;
					$user->firstname = $data['firstname'];
					$user->lastname = $data['lastname'];

					$user->role = "USER";
					$user->save();
					$_SESSION['user'] = $user;
					$app->redirect($app->urlFor('moncompte'));
				} else {
					$app->redirect($app->urlFor('register'));
				}
			}
		});
	}


	/* fonction vue pour la connexion   */
	//TODO : passer cette méthode dans une classe authentification
	public function login()
	{

		$this->app->get('/login', function () {
			$view = new userView('connexion');
			$view->render();
		})->name("login");
	}


	//Permet de d'authentifier/Connecter l'utilisateur
	public function processLogin()
	{
		$this->app->post("/login", function () {
			$app = \Slim\Slim::getInstance();
			$data = $app->request->post();

			$login = $data['login'];
			$pass = $data['pass'];


			if (isset($data['takeCookie'])) {
				//Lorqu'on selectionne se souvenir de moi un cookie est créé
				//Pour une durée de 1 jour
				setcookie('login', $login, time() + (86400 * 30), '/');
				setcookie('pass', $pass, time() + (86400 * 30), '/');
			}

			// On cherche l'utilisateur par son login
			$user = User::where('login', '=', $login)->first();

			if ($user) {

				//Verification si le mot de passe est correct
				if (password_verify($pass, $user['password'])) {
					//stock les information de la session dans une variable de session
					$_SESSION['user'] = $user;
					$app->redirect($app->urlFor('moncompte')); //Changer en Home
				} else {
					//Il faut afficher un message indiquant que soit 
					//Le mdp est incorrect ou l'utilisateur
					$_SESSION['flash'] = "Utilisateur inconnu ou mot de passe incorrect";

					$app->redirect($app->urlFor('login'));
				}
			} else {
				$app->redirect($app->urlFor('login'));
			}
		});
	}


	//TODO : utiliser la variable de session au lieu des id
	public function editUser()
	{
		$this->app->get("/editUser", function () {

			$id = $_SESSION['user']['user_id'];
			$user = User::where('user_id', '=', $id)->first();
			$_SESSION['currentUser'] = $user;

			$view = new userView('editUser');
			$view->render();
		});
	}

	public function processEditUser()
	{
		$this->app->post('/editUser', function () {
			$app = \Slim\Slim::getInstance();
			$data = $app->request->post();
			$id = $_SESSION['user']['user_id'];

			if (!empty($data['pass1']) && !empty($data['pass2'])) {

				if ($data['pass1'] === $data['pass2']) {
					//On hash le mot de passe
					$pass = password_hash($data['pass1'], PASSWORD_DEFAULT);

					//Insertion des informations dans la base de donnée

					$user = User::where('user_id', '=', $id)->first();
					$user->password = $pass;
					$user->firstname = $data['firstname'];
					$user->lastname = $data['lastname'];
					$user->save();
					//on redirige vers la saisie
					$app->redirect($app->urlFor('login'));
				} else {
					$app->redirect($app->urlFor('register'));
				}
			}
		});
	}

	public function logout()
	{
		$this->app->get("/logout", function () {
			session_destroy();
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor('homepage'));
		});
	}

	public function deleteAccount()
	{
		$this->app->get('/deleteAccount', function () {

			$id = $_SESSION['user']['user_id'];
			$user = User::where('user_id', '=', $id)->first();

			//On recherche les listes ratachée à un utilisateur
			$listes = $user->listes()->get();
			foreach ($listes as $liste) {

				$items = $liste->items()->get();

				foreach ($items as $item) {
					$itemToDelete = Item::find($item->id);
					$itemToDelete->delete();
				}
			}

			foreach ($listes as $liste) {

				$liste = Liste::find($liste->no);
				$liste->delete();
			}

			$userDelete = User::find($id);
			$userDelete->delete();

			$this->app->redirect($this->app->urlFor('homepage'));
		});
	}

	/**
     * Affiche l'ensemble des participation
     */
    public function getParticipation(){
        $this->app->get("/userParticipation", function(){

            $id = $_SESSION['user']['user_id'];
			$u = User::where('user_id','=',$id)->first();
			
			$_SESSION['currentUser']= $u;
			$view = new userView('getParticipation');
			$view->render();

        });
	}
	
	/**
	 * Lie Une liste a un compte
	 * 
	 */

    public function linkListe(){
        $this->app->get("/userLinkListe", function(){
			
			$view = new userView('linkliste');
			$view->render();

        })->name('linkliste');
	}
	
	public function processlinkListe(){
        $this->app->post("/userLinkListe", function(){
			$app = \Slim\Slim::getInstance();

			$id = $_SESSION['user']['user_id'];
			$data = $app->request->post();
			$editToken = $this->StringInputCleaner($data['token']);
			
			if(!empty($editToken)){
				$liste = Liste::where('edit_token', 'LIKE', $editToken)->first();
				if($liste === NUll){
					$_SESSION['flash']= "Probleme";
					$this->app->redirect($this->app->urlFor('linkliste'));
				}

				$liste->user_id = $id;
				$liste->save();
				$this->app->redirect($this->app->urlFor('userList'));
			}
			else {
				$_SESSION['flash']= "Probleme";
				$this->app->redirect($this->app->urlFor('linkliste'));
			}
			
        });
    }
}
