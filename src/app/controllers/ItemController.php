<?php
namespace Controllers;

error_reporting(-1);
ini_set('display_errors',1);
use Models\Item;
use Vues\ItemView;
use Models\Liste;

class itemController  
{
    public $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    //Fonction permettant d'eviter les injections sql 
    function StringInputCleaner($data){
        //Enleve l'espace avant et après
        $data = trim($data); 
        //enlève les slashes
        $data = stripslashes($data); 
        $data=(filter_var($data, FILTER_SANITIZE_STRING));
        return $data;
    }	

    public function getItems(){
        /**
         * affichage des items 
         */
        $this->app->get('/items', function () {
            $items = Item::select('id', 'liste_id','nom','descr')
                    ->get();
            $showItem = new ItemView($items, 'ALL');
            $showItem->render();    

        })->name('listeItems');

    }

    public function getOneItem(){
        $this->app->get('/item/:id', function($id)  {
            $item = Item::find($id);
            $showItem = new ItemView($item, 'ONE');
            $showItem->render();

        });
    }

    /**
     * Formulaire permettant d'entrer les informations sur un item
     * 
     */
    public function createItem(){
        $this->app->get('/newItem/:token/:id', function($token,$id) {

            
            $_SESSION['id']=$id;
            $_SESSION['token'] = $token;
            $view = new ItemView("createItem");
            $view->render();

    })->name("addItem");
}

  


    public function processCreateItem(){
        $this->app->post('/newItem/:token/:id', function ($token,$id){
            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();
            $nom = $this->StringInputCleaner($data['nom']);
            $descr = $this->StringInputCleaner($data['descr']);
            $price = $this->StringInputCleaner($data['price']);
            //Doit être optionnel verifier 
            $image = $this->StringInputCleaner($data['image']);
            if(empty($image)){
                $image ="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";
            }
          
            if(!empty($nom) && !empty($descr) && !empty($price)){
                $item = new Item();
                $item->liste_id = $id;
                $item->nom = $nom;
                $item->descr = $descr;
                $item->tarif = $price;
                $item->url = $image;
                $item->save() ;
                echo "Item crée avec succès";
                //Pour tout utilisateur rediriger vers le homepage
                    $app->redirect($app->urlFor("oneListe", array('token' => $token)));
                
                
            }
            else{
                $_SESSION['flash']="Error";
                $app->redirect($app->urlFor("addItem", array('token'=>$token,'id' => $id)) );
            }

            

        });
            
    }

    public function deleteItem(){
        $this->app->get('/deleteItem/:token/:id', function($token,$id) {

             $app = \Slim\Slim::getInstance() ;  

            
            $item = Item::find($id);
            $item->delete();

            $this->app->redirect($this->app->urlFor("oneListe", array('token' => $token)) );
        });
    }

    public function editItem(){
        $this->app->get('/userEditItem/:token/:id', function($token, $id) {
            $item = Item::find($id);

            $_SESSION['id']=$id;
            $_SESSION['token']=$token;
            $_SESSION['item']= $item;

            $view = new ItemView("editItem");
            $view->render();

        });
    }
    /**
     * Veille a ce que la suppression ne soit pas possible si reservé
     */
    public function processEditItem(){
        $this->app->post('/userEditItem/:token/:id', function ($token, $id){
            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();
            $nom = $this->StringInputCleaner($data['nom']);
            $descr = $this->StringInputCleaner($data['descr']);
            $price = $this->StringInputCleaner($data['price']);

            //Verification pour les images. Celui ci doit etre une image
            $headers = get_headers($data['image'], 1);
            if (strpos($headers['Content-Type'], 'image/') !== false) {
                $image = $data['image'];
            } else {
                $image ="";
            } 

            if(!empty($nom) || !empty($descr)){
                $item = Item::find($id);
                $item->nom = $nom;
                $item->descr = $descr;
                $item->tarif = $price;
                $item->url = $image;
                $item->save() ;
                $app->redirect($app->urlFor("oneListe", array('token' => $token)) );
            }
            else{
                echo "Erreur lors de la création";
            }

            

        });
            
    }


    /**
     * Permet à un utilisateur de reserver un item
     */
    public function participateItem(){
        $this->app->get('/participate/:token/:id', function($token,$id) {
            $name =$_SESSION['user']['login'];

            $_SESSION['id'] = $id;
            $_SESSION['name'] = $name;
            $_SESSION['token'] = $token;

            $view = new ItemView("participateItem");
            $view->render();

        })->name('participation');
    }

    public function processParticipateItem(){
        $this->app->post('/participate/:token/:id', function($token,$id) {
            $item = Item::find($id);

            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();
            $nom = $this->StringInputCleaner($data['nom']);
            $msg = $this->StringInputCleaner($data['msg']);
            $userId= $_SESSION['user']['user_id'];

            if(!empty($nom) && !empty($msg)){
                $item = Item::find($id);
                $item->booked = 1;
                $item->participation_user = $userId;
                $item->participation_name = $nom;
                $item->participation_msg = $msg;
                $item->save() ;
               // $app->redirect($app->urlFor('userList') );
                $this->app->redirect($this->app->urlFor("oneListeParticipate", array('token' => $token)) );
            }
            else{
                $app->redirect($app->urlFor('participation') );
            }
        });
    }

    /**
     * Gestion de l'upload de ficher
     */
    public function uploadFile(){
        $this->app->get('/upload/:token/:id', function($token,$id) {
            echo <<<EOF

            <form action="../../upload/$token/$id" method="post" enctype="multipart/form-data">
            Choisissez une image à upload:
            <input type="file" name="fileToUpload" id="fileToUpload" required>
            <input type="submit" value="Upload Image" name="submit">
            </form>
            <a href="./homepage" >Annuler</a>
EOF;
            
        })->name('fileUpload');
    }


    /**
     * Gestion enregistrement dans la base de donnée des images uploader
     */
    public function processUploadFile(){
        $this->app->post('/upload/:token/:id',function($token,$id){
            //Lieu ou sera placer les image
            $target_dir = "src/web/img/";
          
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;

            //Nom de l'extention du fichier
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Verifie si le fichier est vrai
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }
            
            //Verifie si l'image existe déjà dans la liste des fichiers
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            //Verifie la taille
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            //Uploade seulement des fichiers de type jpg
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // Si tout est bon, upload le fichier
            } else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    $item = Item::find($id);
                    $item->img = basename( $_FILES["fileToUpload"]["name"]);
                    $item->save();
                    $this->app->redirect($this->app->urlFor("oneListe", array('token' => $token)));
                } else {
            
                    $this->app->redirect($this->app->urlFor('fileUpload'));
                }
            }

        });
    }

}
