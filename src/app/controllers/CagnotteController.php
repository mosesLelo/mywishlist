<?php
/**
 * Cette classe a pour but de gerer la cagnotte
 */
namespace Controllers;

use Models\Cagnotte;
use Models\Item;
use Vues\CagnotteView;

class cagnotteController{

    public $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

  

    public function getCagnottes(){
        /**
         * affichage des items 
         */
        $this->app->get('/cagnottes', function () {
            $cagnotte = Cagnotte::select()
                    ->get();
               

            
        })->name('allCagnottes');

    }


    public function getCagnottesParticipant(){
        /**
         * affichage des items 
         */
        $this->app->get('/CagnotteParticipant/:id', function ($id) {
            $cagnotte = Cagnotte::select()
                    ->get();
            
            $cagnotte = new Cagnotte();

            echo "En construction";
               

            
        })->name('cagnottesParticipant');

    }

    //Creer la cagnitte 
    public function createCagnotte(){
        
        $this->app->get('/createCagnotte/:token/:id', function ($token,$id) {
            $item = Item::find($id);

            $item->cagnotted = 1;
            $item->save();

            $this->app->redirect($this->app->urlFor("oneListe", array('token' => $token)) ); 
        });

    }

    public function formParticipation(){
        $this->app->get('/participateCagnotte/:token/:id', function ($token,$id) {
            $item = Item::find($id);
            $sum = $item->tarif;

            $itemsCagnotte = $item->cagnottes()->get();

            //Determine le reste a payer
            foreach ($itemsCagnotte as $cagno) {
                $sum -= $cagno->montant;
            }
            $_SESSION['sum'] = $sum;
            $_SESSION['item'] = $item;
            $_SESSION['id'] = $id;
            $_SESSION['token'] = $token;
            
            if(isset($_SESSION['user'])){
                $view = new CagnotteView('formParticipation');
                $view->render();
            }else {
                $error = new RestrictionView('stop');
                $error->render();
            }
          
        })->name('cagnotte');
    }


    public function processFormParticipation(){
        $this->app->post('/participateCagnotte/:token/:id', function ($token,$id) {

            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();

            $userId = $_SESSION['user']['user_id'];
            $montant = $data['montant'];

            $item = Item::find($id);
            $sum = $item->tarif;

            $itemsCagnotte = $item->cagnottes()->get();

            //Determine le reste a payer
            foreach ($itemsCagnotte as $cagno) {
                $sum -= $cagno->montant;
            }

            if($montant <= $sum){
                //Creation de la cagnotte 
                //Pour l'item en parametre
                $cagnotte = new Cagnotte();
                $cagnotte->user_id = $userId;
                $cagnotte->item_id = $id;
                $cagnotte->montant = $montant;
                $cagnotte->save();
                $this->app->redirect($this->app->urlFor("oneListeParticipate", array('token' => $token)) ); 
            }
            else{
                $this->app->redirect($this->app->urlFor("cagnotte", array('token' => $token, $id))); 
            }

            

        });
    }

}
?>

