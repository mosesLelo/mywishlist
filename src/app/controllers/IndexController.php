<?php
/**
 * Cette classe a pour but d'etre la page principale
 */
namespace Controllers;
use Vues\indexView; 

class IndexController{

    public $app;

    /*constructeur*/

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function getIndex(){
        $this->app->get('/', function () {
        

            
                $view = new indexView('guest');

           
           $view->render(); 
         })->name('homepage');
    }

}

?>
