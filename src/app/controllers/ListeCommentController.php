<?php

namespace Controllers;

use Models\ListeComment;
use Models\Liste;

class ListeCommentController  
{
    public $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

     //Fonction permettant d'eviter les injections sql 
     function StringInputCleaner($data){
        //Enleve l'espace avant et après
        $data = trim($data); 
        //enlève les slashes
        $data = stripslashes($data); 
        $data=(filter_var($data, FILTER_SANITIZE_STRING));
        return $data;
    }	

    public function processWriteComment(){
        $this->app->post('/listeAddComment/:token', function ($token) {

            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();
            $liste = Liste::where('token','LIKE',$token)->first();
            $nom = $data['nom'];
            $commentInfo = $data['comment'];
            $userId = "";
  
            $comment = new ListeComment();

            if(isset($_SESSION['user'])){
                $userId =$_SESSION['user']['user_id'];
            }


            if(!empty($nom) && !empty($commentInfo)){
                $comment = new ListeComment();
                $comment->user_id = $userId;
                $comment->list_id = $liste->no;
                $comment->username = $nom;
                $comment->comment = $commentInfo;
                $comment->save();
                $app->redirect($app->urlFor("oneListeParticipate", array('token' => $token)) );  
            }else{
                $app->redirect($app->urlFor("moncompte"));  
            }
        });
    }
}

?>

