<?php
/**
 * Cette classe a pour but d'etre de gérer la page pour la création d'une liste
 */
namespace Controllers;
use Models\Liste;
use Models\User;
use Vues\createListView;
use Models\Item;

class ListController{

    public $app;

    /*constructeur*/

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Permet d'encoder correctement
     */
    function base64url_encode($s) {
        return str_replace(array('+', '/'), array('-', '_'), base64_encode($s));
    }

     //Fonction permettant d'eviter les injections sql 
     //TODO: mettre ça dans une classe globale
     function StringInputCleaner($data){
        //Enleve l'espace avant et après
        $data = trim($data); 
        //enlève les slashes
        $data = stripslashes($data); 
        $data=(filter_var($data, FILTER_SANITIZE_STRING));
        return $data;
    }	


    public function createListUser(){
        $this->app->get('/createListe', function () {
            
           $view = new createListView('creerList'); 
           $view->render(); 
         })->name('ListCreateUser');
    }

    public function processCreateListUser(){
        $this->app->post('/createListe', function (){
            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();
            $title = $this->StringInputCleaner($data['name']);
            $descr = $this->StringInputCleaner($data['comment']);
            $date = $data['Date_expiration:'];
            //Token de modification
                  
            

            $editToken =md5($data['name']); //$this->base64url_encode(md5($date['name']));
            
            //Token de partage
            $viewToken = md5($data['name']);//$this->base64url_encode($date['name']);

            if(!empty($title) && !empty($descr)){
                $liste = new Liste();
                if(isset($_SESSION['user'])){
                    $liste->user_id = $_SESSION['user']['user_id'];
                }
                $liste->titre= $title;
                $liste->description = $descr;
                $liste->expiration = $date;
                $liste->token = $viewToken;
                $liste->edit_token = $editToken;
                $liste->save() ;
                $this->app->redirect($this->app->urlFor("oneListe", array('token' => $editToken)) );
               
            }
            else{
                /**
                 * Todo gerer erreur lors de la creation
                 */
                $_SESSION['flash']= "Erreur lors de la creation de liste";
                $app->redirect($app->urlFor('ListCreateUser'));
            }

        });
    }




    /**
     * Form permettant de d'entrer les informations d'une liste 
     * et de l'éditer
     */
    public function editList(){
        $this->app->get('/editListe/:token', function($token) {

            $li = Liste::where('edit_token','LIKE',$token)->first();

            $_SESSION['listEdit'] = $li;

            $view = new createListView('editList');
            $view->render();
    });
}

  
    /**
     * Methode gerant la sauvegarde de la methode dans la base de données
     */
    public function processEditListe(){
        
        $this->app->post('/editListe/:token', function ($token){
            $app = \Slim\Slim::getInstance();
            $data = $app->request->post();
            $title = $this->StringInputCleaner($data['title']);
            $descr = $this->StringInputCleaner($data['descr']);
            $date = $data['date'];
            $sharedList = $data['shared'];
            if(!empty($title) && !empty($descr)){
                $liste = Liste::where('edit_token','LIKE',$token)->first();
                $liste->titre= $title;
                $liste->description = $descr;
                $liste->expiration = $date;
                $liste->shared = $sharedList;
                $liste->save() ;
                
                if(isset($_SESSION['user'])){
                    $app->redirect($app->urlFor('userList') );
                }
                $app->redirect($app->urlFor("oneListe", array('token' => $token)));
            }
            else{
                echo "Erreur lors de la création";
            }
        });
            
    }

    /**
     * Fonction ayant pour but d'obtenir la liste d'un utilisateur
     */
    public function getAllList(){
        /**
         * affichage de la liste des listes de souhaits publics
         */
        $this->app->get('/userListe', function () {
            $id = $_SESSION['user']['user_id'];
            $u = User::where('user_id','=',$id)->first();

            $_SESSION['userListes'] = $u->listes()->get();

            $view = new createListView('showList');

            $view->render();

         })->name('userList');
    }


    /**
     * Affichage de la liste des items d'une liste de souhaits pour le Utilisateur,
     */
    public function getItemFromList(){
        $this->app->get('/listeDetails/:token', function ($token) {
            $tmp = (string)$token;
            $l = Liste::where('edit_token','LIKE',$tmp)->first();

            $_SESSION['listeDetails'] = $l;
            $_SESSION['currentToken'] = $l->edit_token;
            $_SESSION['tokenPartage'] = $l->token;

            $view = new createListView('listeDetail');
            $view->render();
           
            

        })->name('oneListe');
       
    }

    /**
     * Affichage de la liste des items d'une liste de souhaits pour les partcipants,
     * todo tu dois utiliser verifier le cookie egalement
     */
    public function getItemFromListToParticipate(){
        $this->app->get('/listeDetailsParticipate/:token', function ($token) {
            $l = Liste::where('token','LIKE',$token)->first();
            
            $_SESSION['listeDetails'] = $l;
            $_SESSION['currentToken'] = $token;

            if ( isset($_SESSION['user']) && $l->user_id  === $_SESSION['user']['user_id'] ){
                 $this->app->redirect($this->app->urlFor("oneListe", array('token' => $l->edit_token)));
            }else {
                $view = new createListView('listeDetailsParticipate');
                $view->render();
            }
           
           
        })->name('oneListeParticipate');
       
    }


    /**
     * Methode permettant d'ajouter un item à une liste
     */
    public function addItemToList(){
        $this->app->get('/addItemToListe/:token', function ($token){
            $app = \Slim\Slim::getInstance();
            $liste = Liste::where('edit_token','LIKE',$token)->first();
            $id = $liste->no;
            $app->redirect($app->urlFor("addItem", array('token'=>$token,'id' => $id)) );
        });

    }

    /**
     * Permet de supprimer une liste et ses items 
     */
    public function deleteList(){
        $this->app->get('/deleteListe/:token', function($token){

			$liste = Liste::where('edit_token', 'LIKE', $token)->first();

				$items = $liste->items()->get();

				foreach ($items as $item) {
					$itemToDelete = Item::find($item->id);
					$itemToDelete->delete();
				}
			
				$liste = Liste::find($liste->no);
				$liste->delete();

            if(isset($_SESSION['user'])){
                $this->app->redirect($this->app->urlFor('userList'));
            }
            $this->app->redirect($this->app->urlFor('homepage'));
		
        });
    }

    public function getPublicList(){
        $this->app->get('/public', function(){ 

            $listes = Liste::where('shared', 'like', '1')->get();
            $correctList = array();
            $today = date('Y-m-d');

            //Verifie que la date n'est pas expirée
            foreach ($listes as $l) {
                $listeDate = date($l->expiration);
                if($today <= $listeDate){
                    array_push($correctList,$l);
                }
            }

            $_SESSION['publicListes']  = $correctList;
            $view = new createListView('publicList');
            $view->render();
        });
    }

}
