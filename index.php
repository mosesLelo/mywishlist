<?php
use Models\Database;
use Controllers\UserController;
use Controllers\IndexController;
use Controllers\ListController;
use Controllers\itemController;
use Controllers\ListeCommentController;
use Controllers\cagnotteController;

if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) { return false; }

require 'config.php';
require 'vendor/autoload.php';
new Database();

$app = new \Slim\Slim();
$userC = new UserController($app);
$indexC = new IndexController($app);
$listC = new ListController($app); 
$itemC = new itemController($app);
$commentC = new ListeCommentController($app);
$cagnotteC = new cagnotteController($app);
session_start();

//Se charge de la page d'accueil 
$indexC->getIndex();
/**
 * Gestion de l'utilisateur
 */
//Se charge de l'inscription
$userC->registration();
$userC->processRegistration();

//Se charge de la connexion 
$userC->login();
$userC->processLogin();

//Permet de se deconnecter
$userC->logout();

//Se charge d'afficher page home user
$userC->userHome();

//Se charge de supprimer un utilisateur
$userC->deleteAccount();

//Se charge de modifier un utilisateur
$userC->editUser();
$userC->processEditUser();

//Permet a l'utilisateur de voir ses participation
$userC->getParticipation();

//Permet à l'utilisateur de lier une liste
$userC->linkListe();
$userC->processlinkListe();

/**
 * Gestion de la liste
 */
//Se charge de la création de liste
$listC ->createListUser();
$listC->processCreateListUser();

//Permet de modifier une liste
$listC->editList();
$listC->processEditListe();


//Gere l'affichage des elements d'une liste
$listC->getItemFromList();

//Permet de voir une liste en tant que participant
$listC->getItemFromListToParticipate();


//Permet de voir l'ensemble des liste d'un utilisateur
$listC->getAllList();


//Permet d'ajouter un item dans la liste
$listC->addItemToList();

//Permet de supprimer une liste
$listC->deleteList();

//Affiche les listes publiques
$listC->getPublicList();

/**
 * Gestion des items
 */

 //Se charge d'afficher un item
 $itemC->getOneItem();

 //Se charge de creer un item
 $itemC->createItem();
 $itemC->processCreateItem();

 //Se charge de supprimer un item
 $itemC->deleteItem();

 //Permet de modifer un item
 $itemC->editItem();
 $itemC->processEditItem();

 //Permet de participer sur un item
 $itemC->participateItem();
 $itemC->processParticipateItem();

 //Permet d'uploader un fichier
 $itemC->uploadFile();
 $itemC->processUploadFile();


 /**
  * Gestion des commentaires
  */
  //Se charge de permettr à l'utilisateur d'écrire un commentaire
$commentC->processWriteComment();


/**
 * Gestion de la cagnotte
 */

 //Se charge de creer la cagnotte
 $cagnotteC->createCagnotte();

 //Permete de personnaliser la creation de la cagnotte
 $cagnotteC->formParticipation();
 $cagnotteC->processFormParticipation();



$app->run();//Lance le dispatcher slim
